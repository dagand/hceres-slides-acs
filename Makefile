all: slides.pdf

slides.pdf: ACS.pdf samy.pdf
	pdftk A=ACS.pdf B=samy.pdf cat A1-8 B Aend output slides.pdf

ACS.pdf: ACS.tex
	xelatex ACS.tex

clean:
	rm -f ACS.pdf slides.pdf
